#@ File[] (label="Select files or directories", style='both') inpaths
#@ Float (label="Saturated rate for Microglia channel",min=0,value=2,stepSize=0.5) saturated_micro
#@ Float (label="Saturated rate for Vessel channel",min=0,value=0,stepSize=0.5) saturated_blood
#@ Long (label="Morpho dilation x radius",min=1,value=1) morphoxradi
#@ Long (label="Morpho dilation y radius",min=1,value=1) morphoyradi
#@ Long (label="Morpho dilation z radius",min=1,value=1) morphozradi
#@ Boolean (label="Batch mode",value=true) isbatch

// Measurement of contact between Microgila & Blood vessel in intravital fluorescent microscopy
// Developer: Xingi & Phan

function processDir(dirpath, suffix) {
	// Process files within the folder. 
	// Files must be end by the suffix.
	list = getFileList(dirpath);
	list = Array.sort(list);
	for (i = 0; i < list.length; i++) {
		if(File.isDirectory(dirpath + File.separator + list[i]))
			processDir(dirpath + File.separator + list[i], suffix);
		if(endsWith(list[i], suffix))
			processFile(dirpath + File.separator + list[i]);
	}
}

function processFile(filepath){

	// Close every open image
	close("*");

	// Clear result table
	run("Clear Results");

	// Reset ROI
	roiManager("reset");
	
	print('-----------');

	// Read image
	print('Reading input image...');
	basedir = File.getParent(filepath);
	filename = File.getNameWithoutExtension(filepath);
	print("basedir="+basedir);
	print("filename="+filename);
	open(filepath);
	print('Done.');

	// Smooth image
	print('Smoothing the image by Gaussian...');
	// Question: this is applied to both channels?
	run("Gaussian Blur 3D...", "x=2 y=2 z=2");
	run("Despeckle", "stack");
	rename('inputimg');
	print('Done.');

	// Split channels and rename images
	run("Split Channels");
	selectWindow("C1-inputimg");
	rename("red"); // Blood vessel channel
	selectWindow("C2-inputimg");
	rename("green"); // Microglia channel
	
	print('Blood vessel segmentation...');

	// Subtract red to green channel to reduce wavelength mixing effect
	imageCalculator("Subtract create stack", "red","green");
	close("red");

	// Blood vessel segmentation
	selectWindow("Result of red");
	rename("red");
	
	// Enhance the contrast if need
	if(saturated_blood != 0){
		run("Enhance Contrast...", "saturated="+toString(0.35*saturated_blood)+" process_all use");
	}
	
	// Binarisation
	run("Duplicate...", "duplicate");
	setAutoThreshold("Default dark");
	run("Convert to Mask", "method=Default background=Dark calculate black");

	// Dilation
	selectWindow("red-1");
	run("Morphological Filters (3D)", "operation=Dilation element=Ball x-radius="+toString(morphoxradi)+" y-radius="+toString(morphoyradi)+" z-radius="+toString(morphozradi));
	selectWindow("red-1-Dilation");
	rename("red_mask");
	
	print('Done.');
	
	print('Adding binary outlines to ROI...');

	// Add blood vessel outline at each z to ROI
	// NOTE: getStatistics measures for binary image except presence of Selection
	// We need to remove Selection of the previous slice when running getStatistics of the current slice
	red_end_position=0; // starting position of red mask in ROI manager
	for (i = 1; i <= nSlices; i++) {
		setSlice(i);
		getStatistics(area, mean, min, max, std, histogram);
		if (mean!=0){
			setThreshold(1, 255);
			run("Create Selection");
			roiManager("Add");
			roiManager("Set Color", "red");
			run("Select None");
			red_end_position++;	
		}
	}

	// Rename
	for (i = 0; i < roiManager("count"); i++) {
		roiManager("Select", i);
		roiManager("rename", "Red_ROI_" + (i+1));
	}
	
	print('Done.');
	
	print('Microglia segmentation...');

	// Contrast enhancement of Microglia channel
	selectWindow("green");
	if(saturated_micro!=0){
		run("Enhance Contrast...", "saturated="+toString(0.35*saturated_micro)+" process_all use");
	}
	run("Duplicate...", "duplicate");

	// Microglia segmentation
	setAutoThreshold("Otsu dark");
	run("Convert to Mask", "method=Otsu background=Dark calculate black");
	rename("green_mask");
	
	print('Done.');
	
	print('Adding binary outlines to ROI...');

	// Add microglia outline at each z to ROI
	green_end_position=red_end_position; // starting position of green mask in ROI manager
	for (i = 1; i <= nSlices; i++) {
		setSlice(i);
		getStatistics(area, mean, min, max, std, histogram);
		if (mean!=0){
			setThreshold(1, 255);
			run("Create Selection");
			roiManager("Add");
			roiManager("Set Color", "geen");
			run("Select None");
			green_end_position++;	
		}
	}

	// Rename
	for (i = red_end_position; i < roiManager("count"); i++) {
		roiManager("Select", i);
		roiManager("rename", "Green_ROI_" + (i+1));
	}
	
	print('Done.');
	
	print('Measuring the overlapping between two channels');

	// Contact segmentation
	// Get logical AND by multiplication
	// Here only those pixels are left that are overlapping (logical AND; 1*1 = 1, 0*1 = 0)
	// This will also work for a stack.
	imageCalculator("Multiply create stack", "red_mask", "green_mask");
	rename("logical_and");

	// Add overlap outline at each z to ROI
	for (i = 1; i <= nSlices; i++){
		setSlice(i);
		getStatistics(area, mean, min, max, std, histogram);
		if (mean!=0){
			setThreshold(1, 255);
			run("Create Selection");
			roiManager("Add");
			roiManager("Set Color", "blue");
			run("Select None");
		}
	}

	// Rename
	for (i = green_end_position; i < roiManager("count"); i++){
		roiManager("Select", i);
		roiManager("rename", "overlap_ROI_" + (i+1));
	}
	
	print('Done.');
	
	print('Adding results to table...');

	// Compute total area of blood vessel (red channel)
	nROIS=roiManager("Count");
	j=0;
	sumArea=0;
	for(i=0; i<nROIS; i++){
		roiManager("Select", i);
		name = Roi.getName();	
		if(startsWith(name, "Red_")){
			getStatistics(area, mean, min, max, std, histogram);
			sumArea=sumArea+area;
			setResult("Label", j, name);
			setResult("Area", j, area);
			j++;
		}
	}
	sum_Red_Area=sumArea;
	setResult("Label", j,"sum_Red_area");
	setResult("Area", j, sumArea);

	// Add empty row in table to separate with the next computation
	j++;
	setResult("Label", j, " ");
	setResult("Area", j, " ");

	// Compute total area of microglia (green channel)
	j++;
	sumArea=0;
	for(i=0; i<nROIS; i++){
		roiManager("Select", i);
		name = Roi.getName();	
		if(startsWith(name, "Green_")){
			getStatistics(area, mean, min, max, std, histogram);
			sumArea=sumArea+area;
			setResult("Label", j, name);
			setResult("Area", j, area);
			j++;
		}
	}
	sum_Green_Area=sumArea;
	setResult("Label", j,"sum_Green_Area");
	setResult("Area", j, sumArea);

	// Add empty row in table to separate with the next computation
	j++;
	setResult("Label", j, " ");
	setResult("Area", j, " ");

	// Compute total area of the overlap between microglia and blood vessel
	j++;
	sumArea=0;
	for(i=0; i<nROIS; i++){
		roiManager("Select", i);
		name = Roi.getName();	
		if(startsWith(name, "overlap_")){
			getStatistics(area, mean, min, max, std, histogram);
			sumArea=sumArea+area;
			setResult("Label", j, name);
			setResult("Area", j, area);
			j++;
		}
	}
	sum_Overlap_Area=sumArea;
	setResult("Label", j,"sum_Overlap_Area");
	setResult("Area", j, sumArea);

	// Add empty row in table to separate with the next computation
	j++;
	setResult("Label", j, " ");
	setResult("Area", j, " ");

	// Ratio of contact volume to microglia volume
	j++;
	percentage_contact_to_micro=(sum_Overlap_Area * 100)/sum_Green_Area;
	setResult("Label", j,"percentage_contact_to_micro");
	setResult("Area", j, percentage_contact_to_micro);

	// Ratio of contact volume to blood vessel volume
	j++;
	percentage_contact_to_vessels=(sum_Overlap_Area * 100)/sum_Red_Area;
	setResult("Label", j,"percentage_contact_to_vessels");
	setResult("Area", j, percentage_contact_to_vessels);
	
	print('Done.');
	
	print('Saving measurements to files...');
	
	saveAs("results", basedir+File.separator+'Results_'+filename+".csv");
	
	print('Done.');
	
	print('Saving composite results image to file...');

	// Make composite and save to file
	run("Merge Channels...", "c1=red_mask c2=green_mask c3=logical_and create keep");
	saveAs(basedir+File.separator+"Composite_"+filename);
	
	print('Done.');

	// Close some windows
	close("logical_and");
	close('red-1');
	close("red_mask");
	close("green_mask");
}

// Clear the log window
print("\\Clear");

// Close every open image
close("*");

// Check batch mode
if(isbatch==true){
	setBatchMode(true);
}

// Process all images
// Only file with suffix was processed
suffix = 'tif';
for (i=0;i<inpaths.length;i++) {
	item=inpaths[i];
	if (File.exists(item)) {
		if (File.isDirectory(item)) {
			processDir(item,suffix);
        } else {
			if(endsWith(item, suffix))
				processFile(item);
		}
	}
}

















