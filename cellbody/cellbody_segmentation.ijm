#@ File[] (label="Select files or directories", style='both') inpaths
#@ Short (label="Channel for segmentation",min=1,value=1) channelid
#@ Long (label="Median filter radius",min=0,value=3) medradi
#@ Long (label="Rolling ball radius",min=0,value=20) rollradi
#@ String (choices={"RenyiEntropy","Otsu"},style="radioButtonHorizontal",value="RenyiEntropy",label="Thresholding method") thrmethod
#@ String (visibility=MESSAGE, value="Morphological opening", required=false) msg1
#@ Long (label="Morpho opening x radius",min=0,value=4) morphoxradi
#@ Long (label="Morpho opening y radius",min=0,value=4) morphoyradi
#@ Long (label="Morpho opening z radius",min=0,value=1) morphozradi
#@ String (visibility=MESSAGE, value="Distance transform watershed", required=false) msg2
#@ Short (label="Dynamic",min=0,value=2) dynamic
#@ String (choices={"6","26"},style="radioButtonHorizontal",value="26",label="Connectivity") connectivity
#@ Boolean (label="Batch mode",value=true) isbatch

// Segmentation of Microglia cell body in intravital fluorescent microscopy
// Developer: Xingi & Phan
// Plugins need to be installed: MorphoLibJ (IJPB-plugins), 3D ImageJ Suite, ImageScience

function processDir(dirpath, suffix) {
	// Process files within the folder. 
	// Files must be end by the suffix.
	list = getFileList(dirpath);
	list = Array.sort(list);
	for (i = 0; i < list.length; i++) {
		if(File.isDirectory(dirpath + File.separator + list[i]))
			processDir(dirpath + File.separator + list[i], suffix);
		if(endsWith(list[i], suffix))
			processFile(dirpath + File.separator + list[i]);
	}
}

function processFile(filepath) {
	print('-----------');
	
	// Read image
	print('Reading input image...');
	basedir = File.getParent(filepath);
	filename = File.getNameWithoutExtension(filepath);
	print("basedir="+basedir);
	print("filename="+filename);
	open(filepath);
	print('Done.');

	// Select channel for segmentation
	print('Select channel for segmentation...');
	run("Split Channels");
	selectWindow("C"+toString(channelid)+"-"+filename+".tif");
	// rename("Input");
	print('Done.');
	
	img = getTitle(); 

	run("Duplicate...", "duplicate");

	// Some filters to smooth image
	if(medradi>0){
		print('Applying filter...');
		// run("Despeckle", "stack");
		// run("Gaussian Blur 3D...", "x=2 y=2 z=2");
		// This might be not useful? since Despeckle is the median filter with radius = 1
		run("Median...", "radius="+toString(medradi)+" stack"); 
		print('Done.');
	}
	
	// Background substraction
	if(rollradi>0){
		print('Background substraction...');
		run("Subtract Background...", "rolling="+toString(rollradi)+" stack");
		print('Done.');
	}
	
	// Binarisation with RenyiEntropy
	print('Doing binarization...');
	run("Duplicate...", "duplicate");
	setAutoThreshold("RenyiEntropy dark stack");
	setOption("BlackBackground", true);
	run("Convert to Mask", "method="+thrmethod+" background=Dark calculate black");
	print('Done.');

	print('Running watershed...');
	run("Duplicate...", "duplicate");

	// Morphological opening to reduce processes segments
	if((morphoxradi>0)&(morphoyradi>0)&(morphozradi>0)){
		run("Morphological Filters (3D)", "operation=Opening element=Ball x-radius="+toString(morphoxradi)+" y-radius="+toString(morphoyradi)+" z-radius="+toString(morphozradi));
	}
	
	// Watershed segmentation on distance transform to separate touched bodies
	run("Distance Transform Watershed 3D", "distances=[Borgefors (3,4,5)] output=[16 bits] normalize dynamic="+toString(dynamic)+" connectivity="+connectivity);
	run("glasbey on dark");
	print('Done.');

	print('Saving image labels to tif file...');
	// Save labels image to tif
	saveAs("Tiff", basedir+File.separator+'L_'+filename+".tif");
	// Title of image resulted from watershed
	imglbl = getTitle(); 
	print('Saved name: '+imglbl);
	print('Done.');

	print('Running 3D manager...');
	// Use 3D manager to compute all features e.g. compactness, flatness, volumes, etc
	run("3D Manager Options", "volume surface compactness fit_ellipse 3d_moments integrated_density mean_grey_value std_dev_grey_value feret minimum_grey_value maximum_grey_value centroid_(pix) centroid_(unit) distance_to_surface centre_of_mass_(pix) centre_of_mass_(unit) bounding_box radial_distance closest distance_between_centers=10 distance_max_contact=1.80 drawing=Contour");
	run("3D Manager");
	Ext.Manager3D_Reset();
	
	// Add image labels
	selectWindow(imglbl);
	Ext.Manager3D_AddImage();
	Ext.Manager3D_SelectAll();
	
	// Measure shape features
	print('Saving shape features to csv file...');
	Ext.Manager3D_Measure();
	Ext.Manager3D_SaveResult("M",basedir+File.separator+filename+".csv");
	Ext.Manager3D_CloseResult("M");
	
	// Measure image features
	print('Saving image features to csv file...');
	selectWindow(img);
	Ext.Manager3D_Quantif();
	Ext.Manager3D_SaveResult("Q",basedir+File.separator+filename+".csv");
	Ext.Manager3D_CloseResult("Q");
	
	// Close Manager3D
	Ext.Manager3D_Close();

	selectWindow("C"+toString(channelid)+"-"+filename+".tif");
	run("Z Project...", "projection=[Max Intensity]");

	selectWindow("L_"+filename+".tif");
	run("Z Project...", "projection=[Max Intensity]");
	
	selectWindow("C"+toString(channelid)+"-"+filename+".tif");
	print('Saving segmentation channel to tif file...');
	// Save labels image to tif
	saveAs("Tiff", basedir+File.separator+'S_'+filename+".tif");
	
	print('Done.');
}

// Clear the log window
print("\\Clear");

// Close every open image
close("*");

// Check batch mode
if(isbatch==true){
	setBatchMode(true);
}

// Process all images
// Only file with suffix was processed
suffix = 'tif';
for (i=0;i<inpaths.length;i++) {
	item=inpaths[i];
	if (File.exists(item)) {
		if (File.isDirectory(item)) {
			processDir(item,suffix);
        } else {
			if(endsWith(item, suffix))
				processFile(item);
		}
	}
}



