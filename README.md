# LPS-induced systemic inflammation affects the dynamic interactions of astrocytes and microglia with the vasculature of the mouse brain cortex

We provide in this repository the image analysis scripts to quantify morphological changes of perivascular microglia/astrocytes and their interactions with the brain vasculature using 2-photon laser scanning microscopy (2P-LSM).

You will find in the **folder cellbody** the scripts that compute the cellbody volume 
of migroglia/astrocytes, and in the **folder contact** the script that measures 
the contact ratio between microglia/astrocytes and blood vessels.

If you use the scripts, please kindly cite our paper:

> *Xingi, E.; Koutsoudaki, P.N.N.; Thanou, I.; Phan, M.; Margariti, M.; Scheller, A.; Tinevez, J.; Kirchhoff, F.; Thomaidou, D. LPS-Induced Systemic Inflammation Affects the Dynamic Interactions of Astrocytes and Microglia with the Vasculature of the Mouse Brain Cortex. Preprints.org 2023, 2023030525. https://doi.org/10.20944/preprints202303.0525.v1.*

We provide in the **Documentation.pdf** a short guideline on how to use the scripts.
